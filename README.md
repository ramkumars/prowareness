Symfony Standard Edition
========================

Welcome to the Symfony Standard Edition - a fully-functional Symfony
application that you can use as the skeleton for your new applications.

For details on how to download and get started with Symfony, see the
[Installation][1] chapter of the Symfony Documentation.

What's inside?
--------------
Run these commands before testing the code

composer install  
php app/console assets:install --symlink  
app/console doctrine:schema:update --force


/team is the home page 
