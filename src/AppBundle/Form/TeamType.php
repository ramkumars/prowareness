<?php

namespace AppBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\FileType;

use AppBundle\Entity\Team;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class TeamType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
                ->add('logoUri', FileType::class, array('label' => 'Logo', 'data_class' => null))

                ->add('player', CollectionType::class, array(
                    'entry_type' => PlayerType::class,
                    'allow_add'    => true,
                    'prototype' => true,
                    'by_reference' => false,
                ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Team::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_team';
    }


}
