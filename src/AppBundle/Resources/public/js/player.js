(function ($) {
    'use strict';

    var teamForm = {

        $addPlayerLink : $('<a href="#" class="add_player_link"><span class="glyphicon glyphicon-plus-sign"></span></a>'),
        init: function () {
            var that = this;
            var $collectionHolder = $('ul.players');
            $collectionHolder.append(that.$addPlayerLink);
            $collectionHolder.data('index', $collectionHolder.find(':input').length);
            that.$addPlayerLink.on('click', function(e) {
                e.preventDefault();
                that.addPlayerForm($collectionHolder);
            });
        },
        addPlayerForm: function ($collectionHolder) {
            var that = this;
            var prototype = $collectionHolder.data('prototype');
            var index = $collectionHolder.data('index');
            var newForm = prototype.replace(/__name__/g, index);

            $collectionHolder.data('index', index + 1);

            var $newFormLi = $('<li></li>').append(newForm);
            // add a delete link to the new form
            that.addPlayerFormDeleteLink($newFormLi);
            $newFormLi.find('label').remove();
            $collectionHolder.append($newFormLi);
        },
        addPlayerFormDeleteLink: function ($playerFormLi) {
            var that = this;
            var $removePlayerLink = $('<a href="#"><span class="glyphicon glyphicon-minus-sign"></span></a>');
            if( $playerFormLi.find('div').length == 2){
                $($playerFormLi.find('div')[1]).append($removePlayerLink);
            }else{
                console.log($playerFormLi.find('div')[0]);
                $($playerFormLi.find('div')[0]).append($removePlayerLink);
            }
            $removePlayerLink.on('click', function(e) {
                e.preventDefault();
                $playerFormLi.remove();
            });
        }
    };

    window.teamForm = teamForm;

}) (jQuery);

jQuery(document).ready(function() {
    window.teamForm.init();
});