<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TeamControllerTest extends WebTestCase
{
    public function testCompleteScenario()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/team/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /team/");
        $crawler = $client->click($crawler->selectLink('Create a new entry')->link());

        $form = $crawler->selectButton('Create')->form(array(
            'appbundle_team[name]'  => 'NewTeam'
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check data in the show view
        $this->assertGreaterThan(0, $crawler->filter('td:contains("NewTeam")')->count(), 'Missing element td:contains("NewTeam")');

        // Edit the entity
        $crawler = $client->click($crawler->selectLink('Edit')->link());

        $form = $crawler->selectButton('Update')->form(array(
            'appbundle_team[name]'  => 'LastTeam'
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        $this->assertGreaterThan(0, $crawler->filter('[value="LastTeam"]')->count(), 'Missing element [value="LastTeam"]');

        $client->submit($crawler->selectButton('Delete')->form());
        $crawler = $client->followRedirect();

        $this->assertNotRegExp('/LastTeam/', $client->getResponse()->getContent());
    }
}
