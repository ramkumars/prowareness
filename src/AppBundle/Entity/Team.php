<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Team
 */
class Team
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $logoUri;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $player;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->player = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set logoUri
     *
     * @param string $logoUri
     * @return Team
     */
    public function setLogoUri($logoUri)
    {
        $this->logoUri = $logoUri;

        return $this;
    }

    /**
     * Get logoUri
     *
     * @return string 
     */
    public function getLogoUri()
    {
        return $this->logoUri;
    }

    /**
     * Add player
     *
     * @param \AppBundle\Entity\Player $player
     * @return Team
     */
    public function addPlayer(\AppBundle\Entity\Player $player)
    {
        $this->player[] = $player;
        $player->setTeam($this);

        return $this;
    }

    /**
     * Remove player
     *
     * @param \AppBundle\Entity\Player $player
     */
    public function removePlayer(\AppBundle\Entity\Player $player)
    {
        $this->player->removeElement($player);
    }

    /**
     * Get player
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlayer()
    {
        return $this->player;
    }
}
