<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Player;
use AppBundle\Entity\Team;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class ImageUploadListener
{
    private $uploader;

    public function __construct(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    private function uploadFile($entity)
    {
        if ($entity instanceof Team) {
            $file = $entity->getLogoUri();
        }elseif($entity instanceof Player){
            $file = $entity->getImageUri();
        }

        if (!$file instanceof UploadedFile) {
            return;
        }

        $fileName = $this->uploader->upload($file);

        if ($entity instanceof Team) {
            $entity->setLogoUri($fileName);
        }elseif($entity instanceof Player){
            $entity->setImageUri($fileName);
        }


    }
}